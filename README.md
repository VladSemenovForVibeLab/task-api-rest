# Spring Boot 2.7.17 - Task Manager

## Описание
Данное приложение разработано на Spring Boot 2.7.17 и предназначено для создания, чтения, обновления и удаления (CRUD) задач.

## Требования
- Java 8
- Spring Boot Starter Data JPA
- Starter Web
- Devtools
- H2 Database
- Lombok
- Maven

## Модель задачи
```
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String title;
    private boolean completed;
}
```

## CRUD операции
Методы для CRUD операций с задачами:
- Создание задачи: `POST /api/tasks`
- Получение всех задач: `GET /api/tasks`
- Получение задачи по идентификатору: `GET /api/tasks/{id}`
- Обновление задачи: `PUT /api/tasks`
- Удаление задачи: `DELETE /api/tasks/{id}`

## Запуск приложения
1. Убедитесь, что у вас установлен Java 8 и Maven.
2. Склонируйте репозиторий: `git clone git@gitlab.com:VladSemenovForVibeLab/task-api-rest.git`
3. Перейдите в директорию проекта: `cd taskapi`
4. Соберите проект: `mvn package`
5. Запустите приложение
6. Приложение будет доступно по адресу: `http://localhost:8005`

## База данных
Приложение использует базу данных H2, которая запускается в памяти приложения. Для доступа к БД можно использовать следующие параметры:
- URL: `jdbc:h2:file:./taskAPIDB`
- Имя пользователя: `ADMIN`
- Пароль: `1q2w3e`

## Дополнительная информация
Для разработки были использованы следующие версии зависимостей:
- Spring Boot: 2.7.17
- Spring Boot Starter Data JPA: 2.7.17
- Starter Web: 2.7.17
- Devtools: 2.7.17
- H2 Database: 1.4.200
- Lombok: 1.18.22
- Maven