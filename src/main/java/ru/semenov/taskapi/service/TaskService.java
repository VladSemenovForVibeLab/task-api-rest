package ru.semenov.taskapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.semenov.taskapi.models.Task;
import ru.semenov.taskapi.repository.TaskRepository;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService {
    @Autowired private TaskRepository taskRepository;

    public List<Task> getTasks() {
        return taskRepository.findAll();
    }

    public Task createTask(Task t) {
        return taskRepository.save(t);
    }

    public Optional<Task> getTaskById(Long id) {
        Optional<Task> t =taskRepository.findById(id);
        return t;
    }

    public String deleteTaskById(Long id) {
        taskRepository.deleteById(id);
        return "Task deleted successfully!";
    }
}
