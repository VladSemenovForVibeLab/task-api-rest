package ru.semenov.taskapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.semenov.taskapi.exceptions.TaskNotFoundException;
import ru.semenov.taskapi.models.Task;
import ru.semenov.taskapi.service.TaskService;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class TaskController {
    @Autowired private TaskService taskService;
    @GetMapping("/tasks")
    @ResponseBody
    public List<Task> getTasks(){
        return taskService.getTasks();
    }

    @PostMapping("/tasks")
    @ResponseBody
    public Task createTask(@RequestBody Task t){
        return taskService.createTask(t);
    }

    @GetMapping("/tasks/{id}")
    @ResponseBody
    public Optional<Task> getTaskById(@PathVariable Long id){
        return taskService.getTaskById(id);
    }

    @DeleteMapping("/tasks/{id}")
    @ResponseBody
    public String deleteTaskById(@PathVariable Long id) {
        Task t = taskService.getTaskById(id)
                .orElse(null);
        if (t != null) {
            return taskService.deleteTaskById(id);
        }else {
            throw new TaskNotFoundException(id);
        }
    }

    @PutMapping("/tasks")
    @ResponseBody
    public Task updateTask(@RequestBody Task t){
        return taskService.createTask(t);
    }

    @PatchMapping("/tasks/{id}")
    @ResponseBody
    public Task patchTask(@PathVariable Long id, @RequestBody Map<String,Object> updates){
        Task t = taskService.getTaskById(id)
                .orElse(null);
        if(t != null){
            if(updates.containsKey("title")){
                t.setTitle((String) updates.get("title"));
            }
            if(updates.containsKey("completed")){
                t.setCompleted((Boolean) updates.get("completed"));
            }
            return  taskService.createTask(t);
        }
        else {
            throw new TaskNotFoundException(id);
        }
    }
}
