package ru.semenov.taskapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.semenov.taskapi.models.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task,Long> {

}
